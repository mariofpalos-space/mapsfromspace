import math
import numpy as np
from PIL import Image
from owslib.wms import WebMapService

# Sentinel WMS
wms_url = "https://tiles.maps.eox.at/wms?service=wms&request=getcapabilities"

# Working for 8K or 16K, still fails for others.

#print(list(wms.contents))

# Canary islands
originCoords    =   (28.3, -16) # (lat, lon) in degrees

# Italy
#originCoords    =   (42.5, 11.7) # (lat, lon) in degrees

# New Zealand
# Fails east of NZ, probably have to wrap coordinates after 180!
#originCoords    =   (-42,153.7) # (lat, lon) in degrees

# Nile
#originCoords    =   (25.21, 29.84)

extent          =   40      # degrees squared

resolution      =   4 # K pixels

zoom            =   True
zoomResolution  =   16 #K pixels
ratio           =   4

# Confirmation menu
print(f"Resolution: {resolution}K")
print(f"Extent: {extent} degrees squared")

if zoom:
    print("Requesting zoomed in image.")
    print(f"Zoomed in/out ratio: {ratio}")
    print(f"Zoomed in resolution: {zoomResolution}K")

answer = input("Continue? (Y/N)")

def requestMap(originCoords, extent, resolution):
    # Requests a map from WMS.
    ## Max res for Sentinel is 4K. Higher resolutions require a mosaic.
    
    offset  =   extent / 2
    
    ymin    =   originCoords[0] - offset
    ymax    =   originCoords[0] + offset
    xmin    =   originCoords[1] - offset
    xmax    =   originCoords[1] + offset
    
    img = wms.getmap(
        layers = ["s2cloudless-2020"],
        size = [resolution, resolution],
        srs = "EPSG:4326",
        bbox = [xmin, ymin, xmax, ymax],
        format = "image/png")
    
    return img

def mosaic(originCoords, extent, resolution, filename):
    
    # Amount of tiles required.
    divisions   =   math.ceil(resolution / 4096)

    if (divisions % 2) != 0:
        print("Odd number of divisions, this will not work yet.")
    
    # Width of the tiles in degrees.
    tileWidth   =   extent / divisions
    
    # Resolution of the tiles.
    tileRes     =   int(resolution / divisions)
    
    # Empty mosaic, to be filled with tiles.
    mosaic      =   Image.new('RGB', (resolution, resolution))

    # DEBUG
    print(f"Divisions: {divisions}")
    print(f"Tile width: {tileWidth}")
    print(f"tileRes: {tileRes}")
    
    # LOOP
    # Cells are scanned starting from the top left.
    # Maps are requested and added to the mosaic.
    
    # This works for 8K, but probably not for 16K.
    #topLat = originCoords[0] + (tileWidth / 2)
    #topLon = originCoords[1] - (tileWidth / 2)

    # This seems to work for both!
    topLat = originCoords[0] + (tileWidth / 2) + tileWidth * ((divisions/2) - 1)
    topLon = originCoords[1] - (tileWidth / 2) - tileWidth * ((divisions/2) - 1)


    lat     =   topLat
    lon     =   topLon
    offsetY =   0
    offsetX =   0
    index   =   1
    
    tiles   =  divisions ** 2 

    for row in range(divisions):
        
        for column in range(divisions):
            
            # Request tile.
            print(f"Requesting tile {index}/{tiles}.")
            tile = requestMap((lat, lon), tileWidth, tileRes)
            #saveMap(tile, f"{row}{column}")

            # Add tile to mosaic.
            print("Adding tile to mosaic.") 
            mosaic.paste(Image.open(tile), (offsetX, offsetY))

            # Longitude increases.
            lon += tileWidth

            # So does mosaic X offset.
            offsetX += tileRes

            # And the index.
            index += 1

            """
            # DEBUG
            print(f"offsetX: {offsetX}, offsetY: {offsetY}")
            print(f"lat: {lat}, lon: {lon}")
            """
        
        # Latitude goes down.
        lat -= tileWidth
        # Longitude resets.
        lon = topLon
    
        # Mosaic Y offset increases.
        offsetY += tileRes
        # Mosaic X offset resets.
        offsetX = 0

    print(f"Saving {filename}.png")
    mosaic.save(f"{filename}.png")
    print(f"{filename}.png saved")

def saveMap(img, filename):

    filename = filename + ".png"
    print(f"Saving {filename}")
    out = open(filename, "wb")
    out.write(img.read())
    out.close()



# Working on something! Passing extent to the functions without dividing by two.

if answer.lower() in ["y","yes"]:

    print(f"Connecting to WMS @ {wms_url}")
    wms = WebMapService(wms_url)
    print("Connection successful")
    
    resolutionPX        = int(resolution * 1024)    # int not helping, I think.
    print(f"ResolutionPX: {resolutionPX}")
    zoomResolutionPX    = zoomResolution * 1024

    if resolution <= 4:
        
        print(f"Requesting zoomed out image.")
        zoomedOut   = requestMap(originCoords, extent, resolutionPX)
        saveMap(zoomedOut, "zoomedOut")
        
        #if zoom:
        #    zoomedIn    = requestMap(originCoords, int(extent / (2 * ratio)), resolutionPX)
        #    saveMap(zoomedIn, "zoomedIn")


    elif resolution > 4 and resolution <= 16:

        print("Resolution is above the 4096px limit, a mosaic is required.")
        print("Requesting zoomed out mosaic.")
        mosaic(originCoords, extent, resolutionPX, f"zoomedOut")

        #if zoom:
        #    print("Requesting zoomed in mosaic.")
        #    mosaic(originCoords, int(extent / (2 * ratio)), resolutionPX, f"zoomedIn{resolution}K")

    else:
        print("Wow, vaquero.")


    if zoom and zoomResolution <= 4:

        print(f"Requesting zoomed in image.")
        zoomedIn    = requestMap(originCoords, int(extent / ratio), zoomResolutionPX)
        saveMap(zoomedIn, "zoomedIn")


    elif zoom and zoomResolution > 4 and zoomResolution <= 16:

        print("Resolution is above the 4096px limit, a mosaic is required.")
        print("Requesting zoomed in mosaic.")
        mosaic(originCoords, int(extent / ratio), zoomResolutionPX, f"zoomedIn")

    elif zoom and zoomResolution > 16:
        print("Wow, vaquero.")
        
else:
    
    print("Closing.")
